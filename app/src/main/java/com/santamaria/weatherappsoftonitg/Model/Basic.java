package com.santamaria.weatherappsoftonitg.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by santamae on 11/17/2017.
 */

public class Basic implements Parcelable {

    private int count;
    private ArrayList<City> list;

    public Basic(int count, ArrayList<City> list) {
        this.count = count;
        this.list = list;
    }

    protected Basic(Parcel in) {
        count = in.readInt();
        list = in.createTypedArrayList(City.CREATOR);
    }

    public static final Creator<Basic> CREATOR = new Creator<Basic>() {
        @Override
        public Basic createFromParcel(Parcel in) {
            return new Basic(in);
        }

        @Override
        public Basic[] newArray(int size) {
            return new Basic[size];
        }
    };

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ArrayList<City> getList() {
        return list;
    }

    public void setList(ArrayList<City> list) {
        this.list = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(count);
        dest.writeTypedList(list);
    }
}
