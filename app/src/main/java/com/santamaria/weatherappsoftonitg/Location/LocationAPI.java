package com.santamaria.weatherappsoftonitg.Location;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.santamaria.weatherappsoftonitg.View.MainActivity;

public class LocationAPI implements LocationListener {

    private MainActivity activity;

    public LocationAPI(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            activity.getCitiesData(location.getLatitude(), location.getLongitude());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
