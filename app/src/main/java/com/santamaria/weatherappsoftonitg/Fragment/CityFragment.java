package com.santamaria.weatherappsoftonitg.Fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.santamaria.weatherappsoftonitg.Adapter.CityRecyclerAdapter;
import com.santamaria.weatherappsoftonitg.Model.City;
import com.santamaria.weatherappsoftonitg.R;
import com.santamaria.weatherappsoftonitg.View.MainActivity;

import java.util.ArrayList;

public class CityFragment extends Fragment {

    private ArrayList<City> cityList;
    private CityRecyclerAdapter adapter;
    private RecyclerView.LayoutManager recyclerViewLayoutManager;
    private RecyclerView recyclerView;
    OnCitySelectedListener mCallback;

    public CityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_city, container, false);
        recyclerViewLayoutManager = new LinearLayoutManager(getActivity());

        //find the views in the View
        getViews(view);

        if (getArguments() != null) {

            cityList = (ArrayList<City>) getArguments().get(MainActivity.CITY_LIST_KEY_ARGUMENT);

            //If not empty or null, do Actions.
            if (cityList != null && !cityList.isEmpty()) {

                loadDataInFragment();

            }
        }

        // Inflate the layout for this fragment
        return view;
    }

    public void updateDataInCityFragment(final ArrayList<City> cityList) {

        if (cityList != null) {

            this.cityList = cityList;

            loadDataInFragment();
        }

    }

    private void loadDataInFragment() {

        //Create the Adapter that will be used for the RecyclerView
        createRecyclerAdapter();

        //Set the LayoutManager to the RecyclerView
        recyclerView.setLayoutManager(recyclerViewLayoutManager);

        //Assign the Adapter to the RecyclerView
        recyclerView.setAdapter(adapter);

    }

    private void getViews(View view) {

        recyclerView = view.findViewById(R.id.cityRecyclerView);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mCallback = (OnCitySelectedListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mCallback = null;
    }

    private void createRecyclerAdapter() {
        adapter = new CityRecyclerAdapter(cityList, R.layout.city_item, new CityRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                mCallback.onCitySelected(position);
            }
        });
    }

    public interface OnCitySelectedListener {
        void onCitySelected(int position);
    }


}
