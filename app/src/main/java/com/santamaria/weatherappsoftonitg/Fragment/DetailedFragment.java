package com.santamaria.weatherappsoftonitg.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.santamaria.weatherappsoftonitg.R;
import com.santamaria.weatherappsoftonitg.View.MainActivity;

public class DetailedFragment extends Fragment {

    private TextView tvCityName;
    private TextView tvWeatherDescription;
    private TextView tvTemperature;
    private TextView tvHumidity;
    private TextView tvWindSpeed;
    private LinearLayout lLMainLayout;

    public DetailedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detailed, container, false);

        //Get Views from fragment
        getViews(view);

        //Get Bundle information
        Bundle bundle = getArguments();

        if (bundle != null) {

            //set information on each TextView
            setDataToView(bundle);

        }

        return view;
    }

    public void updateDataInDetailedFragment(Bundle bundle) {

        setDataToView(bundle);
    }

    private void setDataToView(Bundle bundle) {

        tvCityName.setText(bundle.getString(MainActivity.CITY_NAME_KEY_ARGUMENT));
        tvWeatherDescription.setText(bundle.getString(MainActivity.WEATHER_DESCRIPTION_KEY_ARGUMENT));
        tvTemperature.setText(Double.toString(bundle.getDouble(MainActivity.TEMPERATURE_KEY_ARGUMENT)) + "º C");
        tvHumidity.setText(Integer.toString(bundle.getInt(MainActivity.HUMIDITY_KEY_ARGUMENT)) + " %");
        tvWindSpeed.setText(Double.toString(bundle.getDouble(MainActivity.WIND_SPEED_KEY_ARGUMENT)) + " m/s");

        setFragmentVisible();
    }

    private void setFragmentVisible() {
        lLMainLayout.setVisibility(View.VISIBLE);
    }

    private void getViews(View view) {

        tvCityName = view.findViewById(R.id.idCityName);
        tvWeatherDescription = view.findViewById(R.id.idWeatherDescription);
        tvTemperature = view.findViewById(R.id.idTemperature);
        tvHumidity = view.findViewById(R.id.idHumidity);
        tvWindSpeed = view.findViewById(R.id.idWindSpeed);
        lLMainLayout = view.findViewById(R.id.idMainLayout);

    }
}
