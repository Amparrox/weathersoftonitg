package com.santamaria.weatherappsoftonitg.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.santamaria.weatherappsoftonitg.Model.City;
import com.santamaria.weatherappsoftonitg.R;

import java.util.ArrayList;

public class CityRecyclerAdapter extends RecyclerView.Adapter<CityRecyclerAdapter.ViewHolder> {

    private ArrayList<City> cityList;
    private int layout;
    private OnItemClickListener listener;

    public CityRecyclerAdapter(ArrayList<City> cityList, int layout, OnItemClickListener listener) {
        this.cityList = cityList;
        this.layout = layout;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.bind(cityList.get(position), position);

    }

    @Override
    public int getItemCount() {

        int size = 0;

        if (cityList != null) {
            size = cityList.size();
        }

        return size;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView tvTitle;
        TextView tvTemp;

        ViewHolder(View itemView) {
            super(itemView);

            cardView = itemView.findViewById(R.id.idCardView);
            tvTitle = itemView.findViewById(R.id.idCityTitle);
            tvTemp = itemView.findViewById(R.id.idTemp);

        }

        void bind(final City city, final int position) {

            tvTitle.setText(city.getName());
            tvTemp.setText(city.getMain().getTemp() + "º C");
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(position);
                }
            });

        }
    }

    //Interface to communicate with the Fragment about the item clicked in the RecyclerView
    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
