package com.santamaria.weatherappsoftonitg.View;

import android.Manifest;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.santamaria.weatherappsoftonitg.Fragment.CityFragment;
import com.santamaria.weatherappsoftonitg.Fragment.DetailedFragment;
import com.santamaria.weatherappsoftonitg.Location.LocationAPI;
import com.santamaria.weatherappsoftonitg.Model.Basic;
import com.santamaria.weatherappsoftonitg.Model.City;
import com.santamaria.weatherappsoftonitg.R;
import com.santamaria.weatherappsoftonitg.Retrofit.Forecast;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, CityFragment.OnCitySelectedListener {

    private DrawerLayout drawerLayout = null;
    private NavigationView navigationView = null;
    private ProgressBar progressBar = null;

    private final String CITY_FRAGMENT_TAG = "CITY_FRAGMENT_TAG";
    private final String units = "metric";

    public static final String BASIC_DATA_KEY_ARGUMENT = "BASIC_DATA_KEY_ARGUMENT";
    public static final String LAST_POSITION_SELECTED_KEY_ARGUMENT = "LAST_POSITION_SELECTED_KEY_ARGUMENT";
    public static final String CITY_LIST_KEY_ARGUMENT = "CITY_LIST_KEY_ARGUMENT";
    public static final String CITY_NAME_KEY_ARGUMENT = "CITY_NAME_KEY_ARGUMENT";
    public static final String WEATHER_DESCRIPTION_KEY_ARGUMENT = "WEATHER_DESCRIPTION_KEY_ARGUMENT";
    public static final String TEMPERATURE_KEY_ARGUMENT = "TEMPERATURE_KEY_ARGUMENT";
    public static final String HUMIDITY_KEY_ARGUMENT = "HUMIDITY_KEY_ARGUMENT";
    public static final String WIND_SPEED_KEY_ARGUMENT = "WIND_SPEED_KEY_ARGUMENT";

    private LocationAPI myLocation;
    private LocationManager locationManager;
    private final int GPS_PERMISSION = 100;

    private Basic basicData;
    private int lastPositionSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nav_drawer);

        //store the last idx position selected by the user
        lastPositionSelected = -1;

        boolean showDrawer = true;

        if (savedInstanceState != null) {
            basicData = savedInstanceState.getParcelable(BASIC_DATA_KEY_ARGUMENT);
            lastPositionSelected = savedInstanceState.getInt(LAST_POSITION_SELECTED_KEY_ARGUMENT);

            initFragment();

            if (lastPositionSelected > 0 && !isSingleFragment()) {
                onCitySelected(lastPositionSelected);
            }

            showDrawer = false;
        }

        //GetViews
        getViews();

        //ActionBar
        setActionBar();

        //Show the Navigation Drawer
        if (showDrawer) {
            drawerLayout.openDrawer(Gravity.START);
        }

    }

    //Get Views from layout
    private void getViews() {

        //Drawer Layout
        drawerLayout = findViewById(R.id.idDrawerLayout);

        //Navigation View
        navigationView = findViewById(R.id.idNavDrawer);
        navigationView.setNavigationItemSelectedListener(this);

        //Progress Bar
        progressBar = findViewById(R.id.idProgressBar);

    }

    //Enable the ActionBar
    private void setActionBar() {

        ActionBar fragmentManager = getSupportActionBar();

        if (fragmentManager != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_nav_drawer);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_actionbar_items_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.idSetting:
                createAlertDialogSetting();
                break;

            case R.id.idExit:
                createAlertCloseApp();
                break;

            case android.R.id.home:
                drawerLayout.openDrawer(Gravity.START);
        }

        return super.onOptionsItemSelected(item);
    }

    private void createAlertDialogSetting() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.alert_dialog_title);
        builder.setIcon(R.drawable.ic_setting_alert_dialog_icon);
        builder.setMessage(R.string.alert_dialog_message);

        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Thanks for your support.", Toast.LENGTH_SHORT).show();
            }
        });

        builder.create().show();
    }

    private void createAlertCloseApp() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.close_title);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(R.string.close_msg);

        builder.setPositiveButton(R.string.close_btn_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        builder.setNegativeButton(getString(R.string.close_btn_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        boolean closeNavDrawer = false;

        switch (item.getItemId()) {

            case R.id.idCities:

                if (basicData == null) {
                    getCurrentLocation();
                }

                closeNavDrawer = true;
                break;

            case R.id.idAbout:
                startAboutActivity();
                closeNavDrawer = true;
                break;

        }

        if (closeNavDrawer) {
            drawerLayout.closeDrawer(Gravity.START);
        }

        return closeNavDrawer;
    }

    private void startAboutActivity() {

        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);

    }

    public void getCitiesData(double latitude, double longitude) {

        Forecast.doCallGetCities().getCities(latitude, longitude, 40, Forecast.APPID_KEY, units, getLanguage()).enqueue(new Callback<Basic>() {
            @Override
            public void onResponse(Call<Basic> call, Response<Basic> response) {

                if (response.isSuccessful()) {

                    basicData = response.body();
                    progressBar.setVisibility(View.GONE);
                    initFragment();

                }
            }

            @Override
            public void onFailure(Call<Basic> call, Throwable t) {
                progressBar.setVisibility(View.GONE);

                if (getCurrentFocus() != null) {
                    Snackbar snackbar = Snackbar.make(getCurrentFocus(), R.string.call_failure, Snackbar.LENGTH_INDEFINITE);
                    snackbar.setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });

                    snackbar.show();
                }
            }
        });
    }

    private String getLanguage() {

        String lang = Locale.getDefault().getLanguage();

        if (lang.compareToIgnoreCase("es") != 0) {
            lang = "en";
        }

        return lang;
    }

    private boolean isSingleFragment() {

        return findViewById(R.id.fragment_container) != null;

    }

    private void initFragment() {

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

        if (basicData != null && basicData.getList() != null) {
            if (isSingleFragment()) {

                if (fragmentManager.findFragmentByTag(CITY_FRAGMENT_TAG) == null) {
                    CityFragment cityFragment = new CityFragment();

                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(CITY_LIST_KEY_ARGUMENT, basicData.getList());

                    cityFragment.setArguments(bundle);

                    fragmentManager.beginTransaction().add(R.id.fragment_container, cityFragment, CITY_FRAGMENT_TAG).commit();
                }
            } else {

                CityFragment cityFragment = (CityFragment) fragmentManager.findFragmentById(R.id.idFragmentCity);
                cityFragment.updateDataInCityFragment(basicData.getList());

            }
        }
    }

    //Listener from the City Fragment
    @Override
    public void onCitySelected(int position) {

        City city = basicData.getList().get(position);
        lastPositionSelected = position;

        if (city != null) {

            Bundle bundle = createBundleDataToDetailFragment(city);

            if (isSingleFragment()) {
                //Implementation for the second fragment
                DetailedFragment detailedFragment = new DetailedFragment();

                detailedFragment.setArguments(bundle);

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, detailedFragment).addToBackStack(null).commit();
            } else {

                DetailedFragment detailedFragment = (DetailedFragment) getSupportFragmentManager().findFragmentById(R.id.idDetailedFragment);
                detailedFragment.updateDataInDetailedFragment(bundle);

            }
        }

    }

    private Bundle createBundleDataToDetailFragment(City city) {

        Bundle bundle = new Bundle();
        bundle.putString(CITY_NAME_KEY_ARGUMENT, city.getName());

        if (!city.getWeather().isEmpty()) {
            bundle.putString(WEATHER_DESCRIPTION_KEY_ARGUMENT, city.getWeather().get(0).getDescription());
        }

        bundle.putDouble(TEMPERATURE_KEY_ARGUMENT, city.getMain().getTemp());
        bundle.putInt(HUMIDITY_KEY_ARGUMENT, city.getMain().getHumidity());
        bundle.putDouble(WIND_SPEED_KEY_ARGUMENT, city.getWind().getSpeed());

        return bundle;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable(BASIC_DATA_KEY_ARGUMENT, basicData);

        if (lastPositionSelected > 0) {
            outState.putInt(LAST_POSITION_SELECTED_KEY_ARGUMENT, lastPositionSelected);
        }
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();

        if (drawerLayout.isDrawerOpen(Gravity.START)) {
            drawerLayout.closeDrawer(Gravity.START);
        } else if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (locationManager != null && myLocation != null) {

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && checkGPSPermission()) {

                if (basicData == null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1800000, 1000f, myLocation);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1800000, 1000f, myLocation);

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (locationManager != null && myLocation != null) {
            locationManager.removeUpdates(myLocation);
        }
    }

    // ------------------    GPS Code      -----------------------

    private boolean checkGPSPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, GPS_PERMISSION);
                return false;
            }
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == GPS_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                myLocation = new LocationAPI(this);
                getCurrentLocation();
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void getCurrentLocation() {

        if (checkGPSPermission()) {

            if (locationManager == null) {
                locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            }

            if (myLocation == null) {
                myLocation = new LocationAPI(this);
            }

            if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                showGPSAlertDialog();
            } else {

                progressBar.setVisibility(View.VISIBLE);

                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1800000, 1000f, myLocation);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1800000, 1000f, myLocation);
            }

        }
    }

    private void showGPSAlertDialog() {

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.gps_alert_title));
        alertDialog.setMessage(getString(R.string.gps_alert_msg));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });
        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.gps_alert_btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    // ------------------  END  GPS Code      -----------------------
}
