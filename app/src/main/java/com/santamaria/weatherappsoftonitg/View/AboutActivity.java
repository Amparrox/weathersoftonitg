package com.santamaria.weatherappsoftonitg.View;

import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.santamaria.weatherappsoftonitg.R;

public class AboutActivity extends AppCompatActivity {

    private TextView tvCurrentVersion;
    private TextView tvSupportedVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getViews();

        setTitle("About");

        tvCurrentVersion.setText(getVersionInfo());

        String supportedVersions = getString(R.string.supported_versions);
        supportedVersions = " " + supportedVersions.replace("\\n", "\n");

        tvSupportedVersion.setText(supportedVersions);

    }

    private void getViews() {

        tvCurrentVersion = findViewById(R.id.idCurrentVersion);
        tvSupportedVersion = findViewById(R.id.idSupportedVersion);
    }

    //get the current version number and name
    private String getVersionInfo() {
        String version = "";
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return version;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.about_actionbar_items_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.idExit:
                createAlertCloseApp();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void createAlertCloseApp() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(R.string.close_title);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setMessage(R.string.close_msg);

        builder.setPositiveButton(R.string.close_btn_positive, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finishAffinity();
            }
        });

        builder.setNegativeButton(getString(R.string.close_btn_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        builder.create().show();
    }

}
